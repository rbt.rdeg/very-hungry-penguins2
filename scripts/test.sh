#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../

  # shellcheck disable=SC2046
  eval $(opam config env)

  # Testing scripts
  #shellcheck scripts/*.sh

  # Testing OCaml code
  oasis setup  > /tmp/null
  ocaml setup.ml -configure --enable-tests  > /tmp/null
  make test  
  ./test.native
)
